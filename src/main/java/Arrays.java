import stdlib.StdIn;
import stdlib.StdOut;

import java.util.Random;

public class Arrays {

    // Eine Dimension  / Vektor
    int[] zeile = new int[4];

    // Zwei Dimensionen, gleichzeitiges Initialisieren mit Werten
    int[][] tabelle = {
            {1, 2, 3},
            {4, 5, 6}
    };

    // Drei Dimensionen
    int[][][] wuerfel;


    public static void main(String[] args) {

        StdOut.print("Geben Sie einen Wert ein: ");

        // Festlegen der Grösse zur Laufzeit
        int size = StdIn.readInt();
        int[] liste = new int[size];

        for (int j : liste)
            StdOut.print(j + " ");
    }
}
