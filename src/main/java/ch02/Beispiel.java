package ch02;

@SuppressWarnings("all")
class Beispiel {
    int wert;

    int machWas(double para1, Dummy para2) {
        para1 = 0;
        para2.wert = 50;

        return 0; // ein Rückgabewert
    }

    // Innere Klasse
    static class Dummy {
        int wert;
    }

    public static void main(String[] args) {

        Beispiel hugo = new Beispiel();
        Dummy uebergabe = new Dummy();

        int ergebnis;
        double wert = 10.5;

        uebergabe.wert = 100;
        System.out.println();
        System.out.printf(" Parameter vor Aufruf:  %6.1f %4d\n", wert, uebergabe.wert);

        ergebnis = hugo.machWas(wert, uebergabe);
        System.out.printf(" Parameter nach Aufruf: %6.1f %4d\n", wert, uebergabe.wert);
    }
}
