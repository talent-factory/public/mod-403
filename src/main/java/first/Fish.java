package first;

import ch.aplu.jgamegrid.Actor;

public class Fish extends Actor {

    public Fish() {
        super("sprites/nemo.gif");
    }

    public static void main(String[] args) {
        new Fish().act();
    }

    public void act() {
        move();
        if (!isMoveValid())
            turn(180);
    }
}
