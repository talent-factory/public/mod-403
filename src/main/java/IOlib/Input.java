package IOlib;

import stdlib.StdIn;
import stdlib.StdOut;

public class Input {

    /**
     * Diese Funktion liest eine Fliesskommazahl som STDIN. Bei einer ungültigen
     * Eingabe wird der Anwender erneut aufgefordert einen korrekten Wert
     * einzugeben.
     *
     * Bei keiner Eingabe (RETURN) liefet die Funktion den Wert 0.0
     *
     * @param prompt Eingabeaufforderung
     * @return eingelesener Fliesskommawert oder 0.0 bei keiner Eingabe
     */
    public static double readDouble(String prompt) {

        String values = "";
        double result = 0;

        while (true) {
            StdOut.print(prompt);
            try {
                values = StdIn.readLine();
                result = Double.parseDouble(values);
                break;
            } catch (Exception e) {
                if (values.equals(""))
                    break;
            }
        }
        return result;
    }

    public static void main(String[] args) {

        double value = readDouble("Eingabe: ");
        StdOut.println("Deine Eingabe war: " + value);
    }
}
