package exercises;

import stdlib.StdIn;
import stdlib.StdOut;

/**
 * <b>Aufgabe</b>
 *
 * <p>
 * Dieses Programm stellt das Pascal'sche Dreieck dar.
 * </p>
 *
 * <pre>
 * $ java PascalTriangle
 * Enter a number: 5
 *     1
 *    1 1
 *   1 2 1
 *  1 3 3 1
 * 1 4 6 4 1 
 * </pre>
 */
public class PascalTriangle {

    public static void main(String[] args) {

        StdOut.print("Enter a number: ");
        var number = StdIn.readInt();

        // TODO Your code goes here

    }
}
