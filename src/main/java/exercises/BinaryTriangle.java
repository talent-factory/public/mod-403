package exercises;

import stdlib.StdIn;
import stdlib.StdOut;

/**
 * <b>Aufgabe</b>
 * <p>
 * Das Programm solle ein binäres Dreieck erstellen. Die Zeichen '1' und '0'
 * sollen abwechselnd dargestellt werden, wobei wir mit der '1' starten.
 * <p>
 *
 * <pre>
 * $ java BinaryTriangle
 * Enter a number: 7
 * 1
 * 01
 * 010
 * 1010
 * 10101
 * 010101
 * 0101010
 * </pre>
 */
public class BinaryTriangle {

    public static void main(String[] args) {

        StdOut.print("Enter a number: ");
        var number = StdIn.readInt();

        // TODO Your code goes here

    }
}
