package exercises;

import stdlib.StdIn;
import stdlib.StdOut;

/**
 * <b>Aufgabe</b>
 * <p>
 * Einlesen einer ganzen Zahl (int) und ausgeben dieser Zahl in
 * umgekehrter Reihenfolge. Der vorgegebene Code darf nicht verändert
 * werden.
 * <p>
 *
 * <pre>
 * $ java ReverseNumber
 * Enter a number: 673251
 * Reverse of entered number: 152376
 * </pre>
 */
public class ReverseNumber {

    public static void main(String[] args) {

        int number, reverse = 0;

        StdOut.print("Enter a number: ");
        number = StdIn.readInt();

        // TODO Your code goes here

        StdOut.println("Reverse of entered number: " + reverse);
    }
}
