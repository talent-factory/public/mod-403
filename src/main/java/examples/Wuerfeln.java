package examples;

import java.util.Random;

public class Wuerfeln {

    public static void main(String[] args) {

        int summe = 8;
        int wuerfe = 1_000;

        int m, n;
        int bingo = 0;

        Random random = new Random();

        for (int i = 0; i < wuerfe; i++) {
            m = random.nextInt(6); // Zufallszahl 0 <= m < 6
            n = random.nextInt(6);

            m++; // Augenzahl 1. Würfel
            n++; // Augenzahl 2. Würfel

            if (n + m == summe) {
                bingo++;
            }
        }
        System.out.println("Auf " + wuerfe + " Würfe hatte ich " +
                bingo + "x die Summe " + summe);
    }
}
