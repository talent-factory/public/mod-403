package examples;

import ch.aplu.turtle.Turtle;

class TurtleStar {
    public static void main(String[] args) {
        Turtle turtle = new Turtle();
        for (int i = 1; i < 50; i++) {
            turtle.forward(100);
            turtle.left(110);
        }
    }
}
