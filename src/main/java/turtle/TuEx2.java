package turtle;

import ch.aplu.turtle.Turtle;

public class TuEx2 {
    public static void main(String[] args) {

        Turtle john = new Turtle(); // Erstellen des Objektes

        int i = 0;
        while (i < 5) {
            john.forward(20);
            john.right(90);
            john.forward(20);
            john.left(90);
            i = i + 1;
        }
    }
}
