package turtle;

import ch.aplu.turtle.Turtle;

public class TuEx5 {
    public static void main(String[] args) {
        Turtle john = new Turtle();

        for (int i = 0; i < 100; i++) {
            john.forward(2);
            if (i < 50)
                john.left(3.6); // Drehe 3.6 Grad
            else
                john.right(3.6);
        }
    }
}
