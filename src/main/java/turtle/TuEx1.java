package turtle;

import ch.aplu.turtle.Turtle;

@SuppressWarnings("DuplicatedCode")
public class TuEx1 {
    public static void main(String[] args) {

        Turtle john = new Turtle(); // Erstellen des Objektes

        john.forward(20);
        john.right(90);
        john.forward(20);
        john.left(90);

        john.forward(20);
        john.right(90);
        john.forward(20);
        john.left(90);

        john.forward(20);
        john.right(90);
        john.forward(20);
        john.left(90);

        john.forward(20);
        john.right(90);
        john.forward(20);
        john.left(90);
    }
}
