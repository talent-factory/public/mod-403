package turtle;

import ch.aplu.turtle.Turtle;

public class TuEx4 {
    public static void main(String[] args) {

        Turtle john = new Turtle(); // Erstellen des Objektes

        for (int i = 0; i < 5; i++) {
            john.forward(20);
            john.right(90);
            john.forward(20);
            john.left(90);
            i = i + 1;
        }
    }
}
